//
// Created by woniu on 16/1/26.
//

#include "androidso_bifang_com_androidso_MainActivity.h"
//#include "EncryptionMode.h"
//#include "Rijndael.h"
//#include "util.h"
//#include "tea.h"
#include <iostream>
//#include <cstdio>
#include "aes.h"
#include <stdint.h>
#include <cstring>
#include "md5.h"
using namespace std;


//void encrypt (uint32_t v[], uint32_t k[]) {
//    uint32_t v0=v[0], v1=v[1], sum=0, i;           /* set up */
//    uint32_t delta=0x9e3779b9;                     /* a key schedule constant */
//    uint32_t k0=k[0], k1=k[1], k2=k[2], k3=k[3];   /* cache key */
//    for (i=0; i < 32; i++) {                       /* basic cycle start */
//        sum += delta;
//        v0 += ((v1<<4) + k0) ^ (v1 + sum) ^ ((v1>>5) + k1);
//        v1 += ((v0<<4) + k2) ^ (v0 + sum) ^ ((v0>>5) + k3);
//    }                                              /* end cycle */
//    v[0]=v0; v[1]=v1;
//}
//
//void decrypt (uint32_t v[], uint32_t k[]) {
//    uint32_t v0=v[0], v1=v[1], sum=0xC6EF3720, i;  /* set up */
//    uint32_t delta=0x9e3779b9;                     /* a key schedule constant */
//    uint32_t k0=k[0], k1=k[1], k2=k[2], k3=k[3];   /* cache key */
//    for (i=0; i<32; i++) {                         /* basic cycle start */
//        v1 -= ((v0<<4) + k2) ^ (v0 + sum) ^ ((v0>>5) + k3);
//        v0 -= ((v1<<4) + k0) ^ (v1 + sum) ^ ((v1>>5) + k1);
//        sum -= delta;
//    }                                              /* end cycle */
//    v[0]=v0; v[1]=v1;
//}

std::string EncodeAES( const std::string& password, const std::string& data )
{
    AES_KEY aes_key;
    if(AES_set_encrypt_key((const unsigned char*)password.c_str(), password.length() * 8, &aes_key) < 0)
    {
        //assert(false);
        return "";
    }
    std::string strRet;
    std::string data_bak = data;
    unsigned int data_length = data_bak.length();
    int padding = 0;
    if (data_bak.length() % AES_BLOCK_SIZE > 0)
    {
        padding =  AES_BLOCK_SIZE - data_bak.length() % AES_BLOCK_SIZE;
    }
    data_length += padding;
    while (padding > 0)
    {
        data_bak += '\0';
        padding--;
    }
    for(unsigned int i = 0; i < data_length/AES_BLOCK_SIZE; i++)
    {
        std::string str16 = data_bak.substr(i*AES_BLOCK_SIZE, AES_BLOCK_SIZE);
        unsigned char out[AES_BLOCK_SIZE];
        ::memset(out, 0, AES_BLOCK_SIZE);
        AES_encrypt((const unsigned char*)str16.c_str(), out, &aes_key);
        strRet += std::string((const char*)out, AES_BLOCK_SIZE);
    }
    return strRet;
}

std::string DecodeAES( const std::string& strPassword, const std::string& strData )
{
    AES_KEY aes_key;
    if(AES_set_decrypt_key((const unsigned char*)strPassword.c_str(), strPassword.length() * 8, &aes_key) < 0)
    {
        //assert(false);
        return "";
    }
    std::string strRet;
    for(unsigned int i = 0; i < strData.length()/AES_BLOCK_SIZE; i++)
    {
        std::string str16 = strData.substr(i*AES_BLOCK_SIZE, AES_BLOCK_SIZE);
        unsigned char out[AES_BLOCK_SIZE];
        ::memset(out, 0, AES_BLOCK_SIZE);
        AES_decrypt((const unsigned char*)str16.c_str(), out, &aes_key);
        strRet += std::string((const char*)out, AES_BLOCK_SIZE);
    }
    return strRet;
}

JNIEXPORT jstring JNICALL Java_androidso_bifang_com_androidso_MainActivity_workso
        (JNIEnv* env, jobject obj)
{

//    char mingwen[] = "http://www.baifds#@$du.com哈";
//    char miwen_hex[1024];
//    //char miwen_hex[] = "03BF8396CECBF7320A2682AEF43236D47323402464752E636F6DE59388";
//    char result[1024];
//    unsigned char key[] = "fdipzone201314showmethemoney!@#$";
//    AES aes(key);
//    aes.Cipher(mingwen, miwen_hex);
//    aes.InvCipher(miwen_hex, result);
//
//
//
//    //const std::string plainStr("AD DE E2 DB B3 E2 DB B3");
//
////    const std::string plainStr("AD DE E2 DB B3 E2 DB B3");
////    const std::string keyStr("3A DA 75 21 DB E2 DB B3 11 B4 49 01 A5 C6 EA D4");
////    const int SIZE_IN = 8, SIZE_OUT = 8, SIZE_KEY = 16;
////    byte plain[SIZE_IN], crypt[SIZE_OUT], key[SIZE_KEY];
////
////    size_t size_in = hexStringToBytes(plainStr, plain);
////    size_t size_key = hexStringToBytes(keyStr, key);
////
//////    if (size_in != SIZE_IN || size_key != SIZE_KEY)
//////        return -1;
////
////    TEA tea(key, 16, true);
////    tea.encrypt(plain, crypt);
////
////    //tea.decrypt(crypt, plain);
////
//
//    encryptMode *EM;
//    char str[MAX_SIZE];
//    int len;
//    EM = new ECBmode;
//
////
//    char iv[] = "12345678901234561234567890123456";
//    iv[strlen(iv)-1] = 0;
//    EM->setIV(iv);
//
//    char key[] = "qwertyuiopasdfghjklzxcvbnmqwerty";
//    key[strlen(key)-1] = 0;
//    EM->setKey(key);
//
//    char plan[] = "this is the text to encrypt";
//    plan[strlen(plan)-1] = 0;
//    len = strlen(plan);
////
////
//    char * pat = EM->aes.printable(EM->encrypt(plan, len),len);
//char * pat = EM->encrypt(plan, len);
//
////    pat[strlen(pat)-1] = 0;
////    len = strlen(pat) ;
////    pat = EM->decrypt(EM->aes.processable(pat), len);
//
//

//
//    char szHex[33];
//    //One block testing
//    CRijndael oRijndael;
//    oRijndael.MakeKey("abcdefghabcdefgh", "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 16, 16);
//    char szDataIn[] = "aaaaaaaabbbbbbbb";
//    char str[MAX_SIZE];
//    char szDataOut[17] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
//    oRijndael.EncryptBlock(szDataIn, str);

//    uint32_t v[2] = {0x61, 0x61}, k[4] = {0x04, 0x03, 0x02, 0x01};
//    printf("Encrypt And Decrypt:\n");
//    //printf("%08X%08X\n", v[0], v[1]);
//    char str[MAX_SIZE];
//    encrypt(v, k);
//
//    //int i; sscanf(v,"%d",&i);
//    sprintf(str,"%08X%08X\n", v[0], v[1]);
////    printf("%08X%08X\n", v[0], v[1]);


    //const string plainStr("AD DE E2 DB B3 E2 DB B3");
    //const string keyStr("3A DA 75 21 DB E2 DB B3 11 B4 49 01 A5 C6 EA D4");
//
//    const string plainStr("00 00 00 61 00 00 00 61 00 00 00 61 00 00 00 61");
//    const string keyStr("00 00 00 04 00 00 00 03 00 00 00 02 00 00 00 01");
//
//    const int SIZE_IN = 8, SIZE_OUT = 8, SIZE_KEY = 8;
//    byte plain[SIZE_IN], crypt[SIZE_OUT], key[SIZE_KEY];
//
//    size_t size_in = hexStringToBytes(plainStr, plain);
//    size_t size_key = hexStringToBytes(keyStr, key);

//
//    if (size_in != SIZE_IN || size_key != SIZE_KEY)
//        return -1;

//    cout << "Plain: " << bytesToHexString(plain, size_in) << endl;
//    cout << "Key  : " << bytesToHexString(key, size_key) << endl;
//
//    TEA tea(key, 32, true);
//    tea.encrypt(plain, crypt);
//    char str[MAX_SIZE];
    //sprintf(str,"%08X%08X\n", v[0], v[1]);
//    cout << "Crypt: " << bytesToHexString(crypt, SIZE_OUT) << endl;

    //tea.decrypt(crypt, plain);
//    cout << "Plain: " << bytesToHexString(plain, SIZE_IN) << endl;

//    CharStr2HexStr((unsigned char*)str, szHex, 16);


    //const unsigned char cstr_i[] =  "123456";
    //unsigned char *cstr = (char *)a;
    //unsigned char* cstr = (unsigned char*)cstr_i;

    jstring ss = env->NewStringUTF("123456");
    unsigned char *cstr = (unsigned char *) env->GetStringUTFChars(ss,0);

    MD5_CTX context = {0};
    MD5Init(&context);
    MD5Update(&context, cstr, 6);
    unsigned char dest[16] = {0};
    MD5Final(dest, &context);
//    (*env)->ReleaseStringUTFChars(env, jInfo, cstr);

    int i;
    char destination[1024] = {0};
//    for (i = 0; i < 16; i++) {
//        sprintf(destination, "%s%02x", destination, dest[i]);
//    }



    string s = EncodeAES("abcdefgabcdefgabcdefgasd","Hello你好 么么");
    for (i = 0; i < strlen(s.c_str()); i++) {
        sprintf(destination, "%s%02x", destination, s[i]);
    }

    //s = DecodeAES("abcdefgabcdefgabcdefgasd",s);

//    char userstrkey[25]="abcdefgabcdefgabcdefgasd";
//    //userstrkey[strlen(userstrkey)-1] = 0;
//    AES_KEY key;
//    AES_set_encrypt_key((unsigned char *)userstrkey,128,&key);
//    char *buf="Hello你好 么么";
//    unsigned char cipher1[16]={0};
//
//    AES_encrypt((unsigned char *)buf, cipher1, &key);
//
//    AES_set_decrypt_key((unsigned char *)userstrkey,128,&key);
//    AES_decrypt(cipher1,cipher1,&key);


//    AES_set_decrypt_key((unsigned char *)userstrkey,128,&key);
//    AES_decrypt(cipher1,cipher1,&key);

//    if (memcmp(buf, cipher1, strlen(buf)) == 0)
//    {
//        std::cout<<"Success, same!"<<std::endl;
//    }


    //const char* pat = (const char*)bytesToHexString(crypt, SIZE_OUT).c_str();

    //const char* pat = (const char*)s.c_str();
    //mingwen
    const char* pat = destination;
    jclass strClass = env->FindClass("java/lang/String");
    jmethodID ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
    jbyteArray bytes = env->NewByteArray(strlen(pat));
    env->SetByteArrayRegion(bytes, 0, strlen(pat), (jbyte*)pat);
    jstring encoding = env->NewStringUTF("utf-8");
    jstring tmp = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);


    //return env->NewStringUTF( "Hello from JNI !  Compiled with ABI .");
    return tmp;
}

