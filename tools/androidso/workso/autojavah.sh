    #!/bin/sh
    export ProjectPath=$(cd "../$(dirname "$1")"; pwd)
    export TargetClassName="androidso.bifang.com.androidso.MainActivity"

    export SourceFile="${ProjectPath}/app/src/main/java"
    export TargetPath="${ProjectPath}/workso/src/main/jni"

    cd "${SourceFile}"
    javah -d ${TargetPath} -classpath "${SourceFile}" "${TargetClassName}"
    echo -d ${TargetPath} -classpath "${SourceFile}" "${TargetClassName}"