package com.bifang.tool;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


public class Main {

    public static void main(String[] args) {
        //System.out.println("Hello, World!");

        Main m = new Main();
        if(args[0].equals("sha_file")){
            System.out.println(m.ShaFile(args[1]));
        }
        if(args[0].equals("crc32_file")){
            System.out.println(m.Crc32File(args[1]));
        }
    }


    //crc32
    private String Crc32File(String filePath){
        String dexCrc = "";
        try
        {
            ZipFile zipfile = new ZipFile(filePath);
            ZipEntry dexentry = zipfile.getEntry("classes.dex");
            dexCrc = Long.toString(dexentry.getCrc());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return dexCrc;
    }

    //哈希值
    private String ShaFile(String filePath){
        MessageDigest msgDigest = null;
        String sha = "";
        try {
            msgDigest = MessageDigest.getInstance("SHA-1");
            byte[] bytes = new byte[1024];
            int byteCount;
            FileInputStream fis = new FileInputStream(new File(filePath));
            while ((byteCount = fis.read(bytes)) > 0)
            {
                msgDigest.update(bytes, 0, byteCount);
            }
            BigInteger bi = new BigInteger(1, msgDigest.digest());
            sha = bi.toString(16);
            fis.close();
            //这里添加从服务器中获取哈希值然后进行对比校验
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sha;
    }
}

