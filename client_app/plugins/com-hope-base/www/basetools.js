cordova.define("com-hope-base.basetools", function(require, exports, module) {
    var exec = require('cordova/exec');
    module.exports = {
        encrypt : function(success, error, strArr){
            exec(success, error, "BaseTools", "encrypt", [strArr]);
        },
        savePhotosAlbum : function (success, error, src) {
            exec(success, error, "BaseTools", "savePhotosAlbum", [src]);
        },
        moveTaskToBack : function () {
            exec(function () {
                
            }, function () {
                
            }, "BaseTools", "moveTaskToBack", []);
        }

    };
});

