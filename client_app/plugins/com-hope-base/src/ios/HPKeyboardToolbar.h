#import <Cordova/CDVPlugin.h>

@interface HPKeyboardToolbar : CDVPlugin
- (void) hide:(CDVInvokedUrlCommand*)command;
- (void) show:(CDVInvokedUrlCommand*)command;
@end