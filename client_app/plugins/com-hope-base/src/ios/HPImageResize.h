#import <Cordova/CDV.h>

@interface HPImageResize : CDVPlugin {}

- (void)resize: (CDVInvokedUrlCommand*) command;

@end