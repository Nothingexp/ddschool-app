package com.hope.weixin;

public class PayOrder {
	private String accessToken;
	private String nonceStr;
	private String packeageValue;
	private Long timeStamp;
	private String productArgs;
	private String prepayKey;
	private String prepayId;


	public String getPrepayId() {
		return prepayId;
	}
	public void setPrepayId(String prepayId) {
		this.prepayId = prepayId;
	}

	public String getPrepayKey() {
		return prepayKey;
	}
	public void setPrepayKey(String prepayKey) {
		this.prepayKey = prepayKey;
	}

	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getNonceStr() {
		return nonceStr;
	}
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getPackeageValue() {
		return packeageValue;
	}
	public void setPackeageValue(String packeageValue) {
		this.packeageValue = packeageValue;
	}

	public Long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getProductArgs() {
		return productArgs;
	}
	public void setProductArgs(String productArgs) {
		this.productArgs = productArgs;
	}

}