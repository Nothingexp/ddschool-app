package com.hope.weixin;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.hope.weixin.MD5;
import com.hope.weixin.Util;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import com.tencent.mm.sdk.modelmsg.SendAuth;

import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXTextObject;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class Weixin extends CordovaPlugin{
	public static final String TAG = "Weixin";

	public static final String ERROR_WX_NOT_INSTALLED = "未安装微信";
	public static final String ERROR_ARGUMENTS = "参数错误";

	public static final String KEY_ARG_MESSAGE = "message";
	public static final String KEY_ARG_SCENE = "scene";
	public static final String KEY_ARG_MESSAGE_TITLE = "title";
	public static final String KEY_ARG_MESSAGE_DESCRIPTION = "description";
	public static final String KEY_ARG_MESSAGE_THUMB = "thumb";
	public static final String KEY_ARG_MESSAGE_MEDIA = "media";
	public static final String KEY_ARG_MESSAGE_MEDIA_TYPE = "type";
	public static final String KEY_ARG_MESSAGE_MEDIA_WEBPAGEURL = "webpageUrl";
	public static final String KEY_ARG_MESSAGE_MEDIA_TEXT = "text";

	public static final int TYPE_WX_SHARING_APP = 1;
	public static final int TYPE_WX_SHARING_EMOTION = 2;
	public static final int TYPE_WX_SHARING_FILE = 3;
	public static final int TYPE_WX_SHARING_IMAGE = 4;
	public static final int TYPE_WX_SHARING_MUSIC = 5;
	public static final int TYPE_WX_SHARING_VIDEO = 6;
	public static final int TYPE_WX_SHARING_WEBPAGE = 7;
	public static final int TYPE_WX_SHARING_TEXT = 8;

	protected IWXAPI api;

	protected static CallbackContext currentCallbackContext;

	private String app_id;
	private static String partner_key;
	private static String partner_id;
	private static String app_secret;
	private HashMap<String,PayOrder> payOrderList = new HashMap<String,PayOrder>();

	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		currentCallbackContext = callbackContext;
		if (!api.isWXAppInstalled()) {
			callbackContext.error(ERROR_WX_NOT_INSTALLED);
			return true;
		}

		if (action.equals("share")) {
			return share(args, callbackContext);
		}else if(action.equals("getAccessToken")){
			return getAccessToken();
		}else if(action.equals("generatePrepayId")){
			return generatePrepayId(args);
		}else if(action.equals("sendPayReq")){
			return sendPayReq(args);
		}
        else if(action.equals("sendAuthReq")){
            return sendAuthReq(args);
        }
//        else if(action.equals("sendAuthReq")){
//            return genUserInfo(args);
//        }
		return false;
	}

	private boolean getAccessToken(){
		Log.i(TAG, "pay begin");
		new GetAccessTokenTask().execute();
		return true;
	}

    private boolean generatePrepayId(JSONArray args){
        try {
            JSONObject prepayInfo = args.getJSONObject(0);
            String productArgs = genProductArgs(prepayInfo);
            if(productArgs !=null){
                new GetPrepayIdTask(productArgs).execute();
            }
        } catch (JSONException e) {
            currentCallbackContext.error("参数格式不正确");
            return true;
        }
        return true;
    }

    private String genProductArgs(JSONObject args) {
        StringBuffer xml = new StringBuffer();

        try {
            String	nonceStr = genNonceStr();
            _nonceStr = nonceStr;
            xml.append("</xml>");
            List<NameValuePair> packageParams = new LinkedList<NameValuePair>();
            packageParams.add(new BasicNameValuePair("appid", app_id));
            packageParams.add(new BasicNameValuePair("body", args.getString("body")));
            packageParams.add(new BasicNameValuePair("mch_id", partner_id));
            packageParams.add(new BasicNameValuePair("nonce_str", nonceStr));
            packageParams.add(new BasicNameValuePair("notify_url", args.getString("notifyUrl")));
            packageParams.add(new BasicNameValuePair("out_trade_no",args.getString("tradeNo")));
            packageParams.add(new BasicNameValuePair("spbill_create_ip", Util.getIpAddress()));
            packageParams.add(new BasicNameValuePair("total_fee", "1"));
            packageParams.add(new BasicNameValuePair("trade_type", "APP"));
            String sign = genPackageSign(packageParams);
            packageParams.add(new BasicNameValuePair("sign", sign));
            String xmlstring = toXml(packageParams);

//            PayOrder payOrder = new PayOrder();
//            payOrder.setPrepayKey(partner_key);
//            payOrder.setPrepayId(partner_id);
//            payOrder.setNonceStr(nonceStr);
            //payOrder.set

            return  new String(xmlstring.toString().getBytes(), "ISO8859-1");
        } catch (Exception e) {
            Log.e(TAG, "genProductArgs fail, ex = " + e.getMessage());
            return null;
        }


    }
    private String genPackageSign(List<NameValuePair> params) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < params.size(); i++) {
            sb.append(params.get(i).getName());
            sb.append('=');
            sb.append(params.get(i).getValue());
            sb.append('&');
        }
        sb.append("key=");
        sb.append(partner_key);


        String packageSign = MD5.getMessageDigest(sb.toString().getBytes()).toUpperCase();
        Log.e("orion",packageSign);
        return packageSign;
    }
    private class GetPrepayIdTask extends AsyncTask<Void, Void, Map<String,String>> {

//        private ProgressDialog dialog;
        private String productArgs;

        public GetPrepayIdTask(String productArgs) {
            this.productArgs = productArgs;
        }

        @Override
        protected void onPreExecute() {
            //dialog = ProgressDialog.show(PayActivity.this, getString(R.string.app_tip), getString(R.string.getting_prepayid));
        }

        @Override
        protected void onPostExecute(Map<String,String> result) {
//            if (dialog != null) {
//                dialog.dismiss();
//            }
//            sb.append("prepay_id\n"+result.get("prepay_id")+"\n\n");
//            show.setText(sb.toString());
//
//            resultunifiedorder=result;
            currentCallbackContext.success(result.get("prepay_id"));

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected Map<String,String>  doInBackground(Void... params) {

            String url = String.format("https://api.mch.weixin.qq.com/pay/unifiedorder");
            String entity = productArgs;

            Log.e("orion",entity);

            byte[] buf = Util.httpPost(url, entity);

            String content = new String(buf);
            Log.e("orion", content);
            Map<String,String> xml=decodeXml(content);

            return xml;
        }
    }

    private String toXml(List<NameValuePair> params) {
        StringBuilder sb = new StringBuilder();
        sb.append("<xml>");
        for (int i = 0; i < params.size(); i++) {
            sb.append("<"+params.get(i).getName()+">");


            sb.append(params.get(i).getValue());
            sb.append("</"+params.get(i).getName()+">");
        }
        sb.append("</xml>");

        Log.e("orion",sb.toString());
        return sb.toString();
    }

    public Map<String,String> decodeXml(String content) {

        try {
            Map<String, String> xml = new HashMap<String, String>();
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(new StringReader(content));
            int event = parser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {

                String nodeName=parser.getName();
                switch (event) {
                    case XmlPullParser.START_DOCUMENT:

                        break;
                    case XmlPullParser.START_TAG:

                        if("xml".equals(nodeName)==false){
                            //实例化student对象
                            xml.put(nodeName,parser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                }
                event = parser.next();
            }

            return xml;
        } catch (Exception e) {
            Log.e("orion",e.toString());
        }
        return null;

    }

    protected boolean sendPayReq(JSONArray args){
		Log.i(TAG, "pay begin");
		try {
			JSONObject prepayIdObj = args.getJSONObject(0);
			String prepayId = prepayIdObj.getString("prepayId");
			sendPayReq(prepayId);
		} catch (JSONException e) {
			e.printStackTrace();
			currentCallbackContext.error("参数错误");
			return false;
		}
		return true;
	}

	protected void getWXAPI() {
		if (api == null) {
			app_id = preferences.getString("weixinappid", "");
			api = WXAPIFactory.createWXAPI(webView.getContext(), app_id, true);
			Boolean registered = api.registerApp(preferences.getString("weixinappid", ""));
		}
	}

	protected boolean share(JSONArray args, CallbackContext callbackContext)
			throws JSONException {
		// check if # of arguments is correct
		if (args.length() != 1) {
			callbackContext.error(ERROR_ARGUMENTS);
		}

		final JSONObject params = args.getJSONObject(0);
		final SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = String.valueOf(System.currentTimeMillis());

		if (params.has(KEY_ARG_SCENE)) {
			req.scene = params.getInt(KEY_ARG_SCENE);
		} else {
			req.scene = SendMessageToWX.Req.WXSceneTimeline;
		}

		// run in background
		cordova.getThreadPool().execute(new Runnable() {
			@Override
			public void run() {
				try {
					req.message = buildSharingMessage(params.getJSONObject(KEY_ARG_MESSAGE));
				} catch (JSONException e) {
					e.printStackTrace();
					currentCallbackContext.error(e.getMessage());
				}
				Boolean sended = api.sendReq(req);
				if(sended){
					currentCallbackContext.success();
				}else{
					currentCallbackContext.error("发送失败");
				}
			}
		});
		return true;
	}

	protected WXMediaMessage buildSharingMessage(JSONObject message)
			throws JSONException {
		URL thumbnailUrl = null;
		Bitmap thumbnail = null;

		try {
			thumbnailUrl = new URL(message.getString(KEY_ARG_MESSAGE_THUMB));
			thumbnail = BitmapFactory.decodeStream(thumbnailUrl
                    .openConnection().getInputStream());

		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		WXMediaMessage wxMediaMessage = new WXMediaMessage();
		wxMediaMessage.title = message.getString(KEY_ARG_MESSAGE_TITLE);
		wxMediaMessage.description = message
                .getString(KEY_ARG_MESSAGE_DESCRIPTION);
		if (thumbnail != null) {
			wxMediaMessage.setThumbImage(thumbnail);
		}

		// media parameters
		WXMediaMessage.IMediaObject mediaObject = null;
		JSONObject media = message.getJSONObject(KEY_ARG_MESSAGE_MEDIA);

		// check types
		int type = media.has(KEY_ARG_MESSAGE_MEDIA_TYPE) ? media
                .getInt(KEY_ARG_MESSAGE_MEDIA_TYPE) : TYPE_WX_SHARING_WEBPAGE;
		switch (type) {
		case TYPE_WX_SHARING_APP:
			break;

		case TYPE_WX_SHARING_EMOTION:
			break;

		case TYPE_WX_SHARING_FILE:
			break;

		case TYPE_WX_SHARING_IMAGE:
			break;

		case TYPE_WX_SHARING_MUSIC:
			break;

		case TYPE_WX_SHARING_VIDEO:
			break;

		case TYPE_WX_SHARING_TEXT:
			mediaObject = new WXTextObject();
			((WXTextObject)mediaObject).text = media.getString(KEY_ARG_MESSAGE_MEDIA_TEXT);
			break;

		case TYPE_WX_SHARING_WEBPAGE:
		default:
			mediaObject = new WXWebpageObject();
			((WXWebpageObject) mediaObject).webpageUrl = media
                    .getString(KEY_ARG_MESSAGE_MEDIA_WEBPAGEURL);
		}
		wxMediaMessage.mediaObject = mediaObject;
		return wxMediaMessage;
	}


	// 支付
	private String genPackage(List<NameValuePair> params) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < params.size(); i++) {
			sb.append(params.get(i).getName());
			sb.append('=');
			sb.append(params.get(i).getValue());
			sb.append('&');
		}
		sb.append("key=");
		sb.append(partner_key); // 注意：不能hardcode在客户端，建议genPackage这个过程都由服务器端完成
		// 进行md5摘要前，params内容为原始内容，未经过url encode处理
		String packageSign = MD5.getMessageDigest(sb.toString().getBytes()).toUpperCase();
		return URLEncodedUtils.format(params, "utf-8") + "&sign=" + packageSign;
	}

	private class GetAccessTokenTask extends AsyncTask<Void, Void, GetAccessTokenResult> {

		@Override
		protected void onPreExecute() {
			Log.i(TAG, "获取accessToken");
			Toast.makeText(cordova.getActivity(), "启动微信支付环境", Toast.LENGTH_LONG).show();
		}

		@Override
		protected void onPostExecute(GetAccessTokenResult result) {

			if (result.localRetCode == LocalRetCode.ERR_OK) {
				Log.d(TAG, "onPostExecute, accessToken = " + result.accessToken);
				currentCallbackContext.success(result.accessToken);
			} else {
				currentCallbackContext.error(result.errCode);
			}
		}

		@Override
		protected GetAccessTokenResult doInBackground(Void... params) {
			GetAccessTokenResult result = new GetAccessTokenResult();
			String url = String.format("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s",
					app_id, app_secret);
			Log.d(TAG, "get access token, url = " + url);

			byte[] buf = Util.httpGet(url);
			if (buf == null || buf.length == 0) {
				result.localRetCode = LocalRetCode.ERR_HTTP;
				return result;
			}

			String content = new String(buf);
			result.parseFrom(content);
			Log.d(TAG, "get access token, result = " + content);
			return result;
		}
	}


	private static enum LocalRetCode {
		ERR_OK, ERR_HTTP, ERR_JSON, ERR_OTHER
	}

	private static class GetAccessTokenResult {

		private static final String TAG = "GetAccessTokenResult";

		public LocalRetCode localRetCode = LocalRetCode.ERR_OTHER;
		public String accessToken;
		public int expiresIn;
		public int errCode;
		public String errMsg;

		public void parseFrom(String content) {

			if (content == null || content.length() <= 0) {
				Log.e(TAG, "parseFrom fail, content is null");
				localRetCode = LocalRetCode.ERR_JSON;
				return;
			}

			try {
				JSONObject json = new JSONObject(content);
				if (json.has("access_token")) { // success case
					accessToken = json.getString("access_token");
					expiresIn = json.getInt("expires_in");
					localRetCode = LocalRetCode.ERR_OK;
				} else {
					errCode = json.getInt("errcode");
					errMsg = json.getString("errmsg");
					localRetCode = LocalRetCode.ERR_JSON;
				}

			} catch (Exception e) {
				localRetCode = LocalRetCode.ERR_JSON;
			}
		}
	}

	private static class GetPrepayIdResult {
		private static final String TAG = "GetPrepayIdResult";
		public LocalRetCode localRetCode = LocalRetCode.ERR_OTHER;
		public String prepayId;
		public int errCode;
		public String errMsg;

		public void parseFrom(String content) {
			if (content == null || content.length() <= 0) {
				Log.e(TAG, "parseFrom fail, content is null");
				localRetCode = LocalRetCode.ERR_JSON;
				return;
			}

			try {
				JSONObject json = new JSONObject(content);
				if (json.has("prepayid")) { // success case
					prepayId = json.getString("prepayid");
					localRetCode = LocalRetCode.ERR_OK;
				} else {
					localRetCode = LocalRetCode.ERR_JSON;
				}
				errCode = json.getInt("errcode");
				errMsg = json.getString("errmsg");
			} catch (Exception e) {
				localRetCode = LocalRetCode.ERR_JSON;
			}
		}
	}

	private String genNonceStr() {
		Random random = new Random();
		return MD5.getMessageDigest(String.valueOf(random.nextInt(10000)).getBytes());
	}

	private long genTimeStamp() {
		return System.currentTimeMillis() / 1000;
	}


    private String genAppSign(List<NameValuePair> params) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < params.size(); i++) {
            sb.append(params.get(i).getName());
            sb.append('=');
            sb.append(params.get(i).getValue());
            sb.append('&');
        }
        sb.append("key=");
        sb.append(partner_key);

        //this.sb.append("sign str\n"+sb.toString()+"\n\n");
        String appSign = MD5.getMessageDigest(sb.toString().getBytes()).toUpperCase();
        Log.e("orion", appSign);
        return appSign;
    }

    public static String _nonceStr = "";

	private void sendPayReq(String prepayId) {
		PayOrder payOrder = payOrderList.get(prepayId);
//		if(payOrder == null){
//			currentCallbackContext.error("不存在prepayId对应的支付订单");
//			return;
//		}

		PayReq req = new PayReq();
		req.appId = app_id;
		req.partnerId = partner_id;
		req.prepayId = prepayId;
		req.nonceStr = _nonceStr;
		req.timeStamp = String.valueOf(genTimeStamp());
//		req.packageValue = "Sign=" + payOrder.getPackeageValue();
        req.packageValue = "Sign=WXPay";

		List<NameValuePair> signParams = new LinkedList<NameValuePair>();
		signParams.add(new BasicNameValuePair("appid", req.appId));
//		signParams.add(new BasicNameValuePair("appkey", app_key));
		signParams.add(new BasicNameValuePair("noncestr", req.nonceStr));
		signParams.add(new BasicNameValuePair("package", req.packageValue));
		signParams.add(new BasicNameValuePair("partnerid", partner_id));
		signParams.add(new BasicNameValuePair("prepayid", req.prepayId));
		signParams.add(new BasicNameValuePair("timestamp", req.timeStamp));
		req.sign = genAppSign(signParams);

        //api.registerApp(partner_id);
		// 在支付之前，如果应用没有注册到微信，应该先调用IWXMsg.registerApp将应用注册到微信
		api.sendReq(req);
		//currentCallbackContext.success("{\"partnerId\":\""+req.partnerId+"\",\"prepayId\":\""+req.prepayId+"\"}");
	}

	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		partner_key = preferences.getString("partner_key", "");
		partner_id = preferences.getString("partner_id", "");
		app_secret = preferences.getString("app_secret", "");
		getWXAPI();
		this.onWeixinResp(cordova.getActivity().getIntent());
	}

	private void onWeixinResp(Intent intent) {
		Bundle extras =  intent.getExtras();
		if(extras!=null){
			String intentType = extras.getString("intentType");
            // 返回支付消息
			if("wxPay".equals(intentType)){
				if(currentCallbackContext != null){
					currentCallbackContext.success(extras.getInt("weixinPayRespCode"));
				}
			}
            else if("wxAuth".equals(intentType)){
                if(currentCallbackContext != null) {
                    currentCallbackContext.success(extras.getString("code"));
                }
            }

		}
	}

	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Log.i(TAG, "onNewIntent");
		this.onWeixinResp(intent);
	}


    public boolean sendAuthReq(JSONArray args){
        SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = String.valueOf(genTimeStamp());
        api.sendReq(req);
        return true;
    }

//    public boolean genUserInfo(JSONArray args){
//
//        try{
//            JSONObject obj = args.getJSONObject(0);
//            String code = obj.getString("code");
//            String productArgs = "?data="+java.net.URLEncoder.encode("{\"code\":\""+code+"\"}");
//            new GetTokenTask(productArgs).execute();
//        }catch (Exception e){
//            return false;
//        }
//        return true;
//    }
//
//    private class GetTokenTask extends AsyncTask<Void, Void, JSONObject> {
//
//        private String productArgs;
//        public GetTokenTask(String productArgs) {
//            this.productArgs = productArgs;
//        }
//
//        @Override
//        protected void onPreExecute() {
//        }
//
//        @Override
//        protected void onPostExecute(JSONObject result) {
//            if(result == null){
//                currentCallbackContext.error("授权失败，请稍后重试。");
//            }
//            else{
//                try{
//                    currentCallbackContext.success(result);
//                }catch (Exception e){
//                    currentCallbackContext.error("授权失败，请稍后重试。");
//                }
//                //currentCallbackContext.success(result.get("prepay_id"));
//            }
//        }
//
//        @Override
//        protected void onCancelled() {
//            super.onCancelled();
//        }
//
//        @Override
//        protected JSONObject  doInBackground(Void... params) {
//
//            //String url = String.format("https://api.weixin.qq.com/sns/oauth2/access_token");
//            String url = String.format("http://192.168.1.160/hope/server/index.php/api/user/role/wx_user_info");
//            String entity = productArgs;
//
//            Log.e("orion",entity);
//            byte[] buf = Util.httpGet(url+entity);
//            if(buf == null){
//                return null;
//            }
//            else{
//                String content = new String(buf);
//                Log.e("orion", content);
//                try {
//                    JSONObject json = new JSONObject(content);
//                    return json;
//
//                } catch (Exception e) {
//                    return null;
//                }
//                //xml=decodeXml(content);
//            }
//
//
//        }
//    }


}
