
var exec = require('cordova/exec');

    module.exports = {
        Scene: {
            SESSION:  0, // 聊天界面
            TIMELINE: 1, // 朋友圈
            FAVORITE: 2  // 收藏
        },
        Type: {
            APP:     1,
            EMOTION: 2,
            FILE:    3,
            IMAGE:   4,
            MUSIC:   5,
            VIDEO:   6,
            WEBPAGE: 7
        },
        share: function (message, onSuccess, onError) {
            exec(onSuccess, onError, "Weixin", "share", [message]);
        },
        getAccessToken: function(onSuccess,onError){
            var weixinPluginValue = localStorage.getItem('cordova.weixinPlugin');
            if(weixinPluginValue != null){
                weixinPluginValue = JSON.parse(weixinPluginValue);
                if(new Date().getTime()/1000 - weixinPluginValue.timeStamp/1000 > 7100){
                    this.getRemoteAccessToken(onSuccess,onError);
                }else{
                    if(onSuccess){
                        onSuccess.call(this,weixinPluginValue.accessToken);
                    }
                }
            }else{
                this.getRemoteAccessToken(onSuccess,onError);
            }
        },
        getRemoteAccessToken:function(onSuccess,onError){
            exec(function(accessToken){
                var weixinPluginValue = {
                    accessToken:accessToken,
                    timeStamp:new Date().getTime()
                };
                localStorage.setItem('cordova.weixinPlugin',JSON.stringify(weixinPluginValue));
                if(onSuccess){
                    onSuccess(accessToken);
                }
            }, onError, "Weixin", "getAccessToken", []);
        },
        generatePrepayId: function(payInfo,onSuccess,onError){
            exec(onSuccess, onError, "Weixin", "generatePrepayId", [payInfo]);
        },
        sendPayReq: function(prepayId,onSuccess,onError){
            exec(onSuccess, onError, "Weixin", "sendPayReq", [{"prepayId":prepayId}]);
        }
        ,
        sendAuthReq: function(prepayId,onSuccess,onError){
            exec(onSuccess, onError, "Weixin", "sendAuthReq", []);
        },
        genUserInfo: function(code,onSuccess,onError){
            exec(onSuccess, onError, "Weixin", "genUserInfo", [{"code":code}]);
        }
    };
