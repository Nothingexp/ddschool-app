/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.zongyang.userclient;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hope.app.AppUpgradeService;
import com.hope.app.HttpTask;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.progress.ProgressMonitor;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

//import com.testin.agent.TestinAgent;
public class MainActivity extends CordovaActivity {

    public native static String keystr();
    public native static String netstr(String str);

    private static boolean isZip = true;
    private static boolean isAutoUpdate = true;

    static {
        System.loadLibrary("workso");
    }

    private ProgressBar progressBar;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TestinAgent.init(this,"15ab7b9edd84980e3e150cae0768a824");
        // Set by <content src="index.html" /> in config.xml

        if(isZip){
            try {
                String VERSION_KEY = "versionCode";
                PackageManager packageManager = this.getPackageManager();
                PackageInfo info = packageManager.getPackageInfo(this.getPackageName(), 0);
                int currentVersion = info.versionCode;
                mVersionCode = currentVersion;
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                int lastVersion = prefs.getInt(VERSION_KEY, 0);
                String path = this.getApplicationContext().getFilesDir().getPath();
                //path = "/sdcard/www";
                if (currentVersion > lastVersion) {
                    prefs.edit().putInt(VERSION_KEY, currentVersion).commit();

                    //解压
                    try {
                        File file = new File(path + "/libsqlite");
                        InputStream ins = getClass().getResourceAsStream("/assets/libsqlite.so");
                        OutputStream os = null;
                        try {
                            os = new FileOutputStream(file);
                        } catch (FileNotFoundException e) {
                            //int bytesRead = 0;
                        }
                        int bytesRead = 0;
                        byte[] buffer = new byte[8192];
                        try {
                            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                                os.write(buffer, 0, bytesRead);
                            }
                            os.close();
                            ins.close();
                        } catch (IOException e) {
                        }
                        String str = keystr();
                        ZipFile zipFile2 = new ZipFile(file);
                        if (zipFile2.isEncrypted()) {
                            zipFile2.setPassword(str);
                        }

                        zipFile2.extractAll(path);
                    } catch (ZipException e) {

                    }


                }
                loadUrl("file://" + path + "/www/index.html");

                //自动更新
                if(isAutoUpdate){
                    getServerVer();
                }
                //getServerVer();
            } catch (PackageManager.NameNotFoundException e) {
                //callbackContext.success("N/A");
                //return true;
            }
        }
        else{
            //loadUrl(launchUrl);
            loadUrl("file:///android_asset/www/index.html");
//            super.setIntegerProperty("loadUrlTimeoutValue",10000);
            //loadUrl("http://192.168.1.160/zy_web/index_test.html");

        }
    }

    private int mLatestVersionCode = 0;
    private String mLatestVersionName ;
    private String mLatestVersionUpdate = null;
    private String mLatestVersionUrl;
    private int mLatestMustUpdate = 0;
    public static int mVersionCode;
    public static boolean mShowUpdate = true;


    public boolean getServerVer(){
        try {

            HttpTask task = new HttpTask();
            task.setTaskHandler(new HttpTask.HttpTaskHandler(){
                public void taskSuccessful(String json) {
                    try {
                        JSONObject jsonObj = new JSONObject(json);
                        mLatestMustUpdate = Integer.parseInt(jsonObj.getString("must_update"));
                        mLatestVersionCode = Integer.parseInt(jsonObj.getString("code"));
                        mLatestVersionName = jsonObj.getString("name");
                        mLatestVersionUrl = jsonObj.getString("url");
                        checkNewVersion(true);
                        } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                public void taskFailed() {
                }
            });
            task.execute("http://120.76.165.59/storage/app/version.json");

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void checkNewVersion(boolean isManual){
        if (mVersionCode < mLatestVersionCode
                && (mShowUpdate || isManual)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.check_new_version);
            builder.setMessage(mLatestVersionUpdate);
//            builder.setItems(new String[] { "Item1", "Item2" }, null);
            builder.setPositiveButton(R.string.app_upgrade_confirm, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(MainActivity.this, AppUpgradeService.class);
                    intent.putExtra("downloadUrl", mLatestVersionUrl);
                    startService(intent);
                }
            });
            if(mLatestMustUpdate == 0){
                builder.setNegativeButton(R.string.app_upgrade_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
            }

            AlertDialog alertDialog = builder.create();
            if(mLatestMustUpdate == 1){
                alertDialog.setCancelable(false);
                alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event)
                    {
                        if (keyCode == KeyEvent.KEYCODE_SEARCH)
                        {
                            return true;
                        }
                        else
                        {
                            return false; //默认返回 false
                        }
                    }
                });
            }
            alertDialog.show();
            mShowUpdate = false;
        }

        if (mVersionCode >= mLatestVersionCode && isManual) {
            //Toast.makeText(this, R.string.check_new_version_latest, Toast.LENGTH_SHORT).show();
        }
    }

//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        int action = ev.getAction();
//        // handle three finger swipe
//
//        super.dispatchTouchEvent(ev);
//        return true;
//    }

//    @Override
//    protected void onResume() {
//        JPushInterface.onResume(getApplicationContext());
//        super.onResume();
//
//    }
//
//    @Override
//    protected void onPause() {
//        JPushInterface.onPause(getApplicationContext());
//        super.onPause();
//
//    }

}
