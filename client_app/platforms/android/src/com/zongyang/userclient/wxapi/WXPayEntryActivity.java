package com.zongyang.userclient.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import org.apache.cordova.ConfigXmlParser;
import org.apache.cordova.CordovaPreferences;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler{

	private static final String TAG = "Weixin";
	protected CordovaPreferences preferences;
	private IWXAPI api;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ConfigXmlParser parser = new ConfigXmlParser();
		parser.parse(this);
		preferences = parser.getPreferences();
		preferences.setPreferencesBundle(getIntent().getExtras());

        api = WXAPIFactory.createWXAPI(this, preferences.getString("weixinappid",""));
        api.handleIntent(getIntent(), this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		api.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq req) {
        Log.d(TAG, "onPayFinish, errCode = ");
	}

	@Override
	public void onResp(BaseResp resp) {
		Log.d(TAG, "onPayFinish, errCode = " + resp.errCode);
		if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
			Intent intent;
			try {

                intent = new Intent(this, com.zongyang.userclient.MainActivity.class);
				Bundle bundle=new Bundle();
				bundle.putInt("weixinPayRespCode",  resp.errCode);
				bundle.putString("intentType", "wxPay");
			    intent.putExtras(bundle);
			    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			    Log.i(TAG, "startActivity");
			    startActivity(intent);
                finish();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}