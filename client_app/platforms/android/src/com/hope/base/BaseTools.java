package com.hope.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;

import com.zongyang.userclient.MainActivity;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class BaseTools extends CordovaPlugin {

    CallbackContext callbackContext;
    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext _callbackContext) throws JSONException {
        callbackContext = _callbackContext;
        //final Context context=this.cordova.getActivity();



        if (action.equals("encrypt")) {
            JSONObject inobj = args.getJSONObject(0);
            String instr = inobj.getString("str");
            String str = MainActivity.netstr(instr);
            callbackContext.success(str);
            return true;
        }
        else if(action.equals("moveTaskToBack")){//最小化
            this.cordova.getActivity().moveTaskToBack(true);
        }
        else if(action.equals("savePhotosAlbum")){

            boolean isPermission = true;
            PackageManager pm = this.cordova.getActivity().getPackageManager();
            boolean permission = (PackageManager.PERMISSION_GRANTED ==
                    pm.checkPermission("android.permission.WRITE_EXTERNAL_STORAGE", "com.dasheng.userclient"));
            if (!permission) {
                isPermission = false;
            }


            JSONObject inobj = args.getJSONObject(0);
            String src = inobj.getString("src");
            PhotosAlbum pa = new PhotosAlbum();
            pa.mContext = this.cordova.getActivity();
            pa.isPermission = isPermission;
            pa.setCallBack(new ICallBack(){
                public void postExec(){
                    final Context context = BaseTools.this.cordova.getActivity();

                    //android 4.4+ 禁用此广播
                    //context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"+ Environment.getExternalStorageDirectory())));

                    //改如下
                    context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
                    //此没经过测试
                    //MediaScannerConnection.scanFile(this, new String[]{Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath() + "/" + fileName}, null, null);
                    callbackContext.success();

                }
            });
            pa.save(src);
            return true;
        }

        return false;
    }

}
