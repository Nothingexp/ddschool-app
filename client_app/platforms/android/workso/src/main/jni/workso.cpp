//
// Created by woniu on 16/2/15.
//

#include "com_zongyang_userclient_MainActivity.h"

#include <iostream>
#include "aes.h"
#include <stdint.h>
#include <cstring>
#include "md5.h"
using namespace std;

std::string EncodeAES( const std::string& password, const std::string& data )
{
    AES_KEY aes_key;
    if(AES_set_encrypt_key((const unsigned char*)password.c_str(), password.length() * 8, &aes_key) < 0)
    {
        //assert(false);
        return "";
    }
    std::string strRet;
    std::string data_bak = data;
    unsigned int data_length = data_bak.length();
    int padding = 0;
    if (data_bak.length() % AES_BLOCK_SIZE > 0)
    {
        padding =  AES_BLOCK_SIZE - data_bak.length() % AES_BLOCK_SIZE;
    }
    data_length += padding;
    while (padding > 0)
    {
        data_bak += '\0';
        padding--;
    }
    for(unsigned int i = 0; i < data_length/AES_BLOCK_SIZE; i++)
    {
        std::string str16 = data_bak.substr(i*AES_BLOCK_SIZE, AES_BLOCK_SIZE);
        unsigned char out[AES_BLOCK_SIZE];
        ::memset(out, 0, AES_BLOCK_SIZE);
        AES_encrypt((const unsigned char*)str16.c_str(), out, &aes_key);
        strRet += std::string((const char*)out, AES_BLOCK_SIZE);
    }
    return strRet;
}

std::string DecodeAES( const std::string& strPassword, const std::string& strData )
{
    AES_KEY aes_key;
    if(AES_set_decrypt_key((const unsigned char*)strPassword.c_str(), strPassword.length() * 8, &aes_key) < 0)
    {
        //assert(false);
        return "";
    }
    std::string strRet;
    for(unsigned int i = 0; i < strData.length()/AES_BLOCK_SIZE; i++)
    {
        std::string str16 = strData.substr(i*AES_BLOCK_SIZE, AES_BLOCK_SIZE);
        unsigned char out[AES_BLOCK_SIZE];
        ::memset(out, 0, AES_BLOCK_SIZE);
        AES_decrypt((const unsigned char*)str16.c_str(), out, &aes_key);
        strRet += std::string((const char*)out, AES_BLOCK_SIZE);
    }
    return strRet;
}


JNIEXPORT jstring JNICALL Java_com_zongyang_userclient_MainActivity_netstr
        (JNIEnv* env, jobject obj ,jstring str)
{
    //jstring ss = env->NewStringUTF("123456");
    //unsigned char *cstr = (unsigned char *) env->GetStringUTFChars(str,0);
    const std::string cstr = env->GetStringUTFChars(str,0);
//    MD5_CTX context = {0};
//    MD5Init(&context);
//    MD5Update(&context, cstr, 6);
//    unsigned char dest[16] = {0};
//    MD5Final(dest, &context);

    int i;
    char destination[1024] = {0};

    string s = EncodeAES("uqi9mxr2pngdwuz2ww48a2jm",cstr);
    for (i = 0; i < strlen(s.c_str()); i++) {
        sprintf(destination, "%s%02x", destination, s[i]);
    }

    const char* pat = destination;
    jclass strClass = env->FindClass("java/lang/String");
    jmethodID ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
    jbyteArray bytes = env->NewByteArray(strlen(pat));
    env->SetByteArrayRegion(bytes, 0, strlen(pat), (jbyte*)pat);
    jstring encoding = env->NewStringUTF("utf-8");
    jstring tmp = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

    return tmp;
}


JNIEXPORT jstring JNICALL Java_com_zongyang_userclient_MainActivity_keystr
        (JNIEnv* env, jobject obj)
{

    return env->NewStringUTF("gTIGUu5o4VdpRUn8NxaEog5H0hwi1E7X");
}