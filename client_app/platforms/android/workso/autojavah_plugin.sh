    #!/bin/sh
    export ProjectPath=$(cd "../$(dirname "$1")"; pwd)
    export TargetClassName="com.zongyang.userclient.MainActivity"

    export SourceFile="${ProjectPath}/src"
    export TargetPath="${ProjectPath}/workso/src/main/jni"

    cd "${SourceFile}"
    javah -d ${TargetPath} -classpath "${SourceFile}" "${TargetClassName}"
    echo -d ${TargetPath} -classpath "${SourceFile}" "${TargetClassName}"