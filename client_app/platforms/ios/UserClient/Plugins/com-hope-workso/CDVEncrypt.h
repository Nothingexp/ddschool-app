//
//  CDVEncrypt.h
//  UserClient
//
//  Created by woniu on 16/2/18.
//
//

#ifndef CDVEncrypt_h
#define CDVEncrypt_h

#include <stdio.h>
#include <stdint.h>
#include <cstring>
#include <iostream>

using namespace std;

class CDVEncrypt{
public:
    std::string encrypt(const std::string cstr);
    
private:
    std::string EncodeAES( const std::string& password, const std::string& data);
    std::string DecodeAES( const std::string& strPassword, const std::string& strData);
    std::string replace(std::string str,std::string pattern,std::string dstPattern,int count);
};


#endif /* CDVEncrypt_h */
