//
//  CDVWorkSo.h
//  UserClient
//
//  Created by woniu on 16/2/18.
//
//

#import <Cordova/CDVPlugin.h>

@interface CDVBaseTools : CDVPlugin

@property (nonatomic, strong) NSString *currentCallbackId;

- (void)encrypt:(CDVInvokedUrlCommand*)command;
- (void)savePhotosAlbum:(CDVInvokedUrlCommand*)command;

@end