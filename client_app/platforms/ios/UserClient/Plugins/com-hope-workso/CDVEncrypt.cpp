//
//  CDVEncrypt.cpp
//  UserClient
//
//  Created by woniu on 16/2/18.
//
//

#include "CDVEncrypt.h"

#include "aes.h"


//#include "md5.h"


std::string CDVEncrypt::EncodeAES( const std::string& password, const std::string& data )
{
    AES_KEY aes_key;
    if(AES_set_encrypt_key((const unsigned char*)password.c_str(), password.length() * 8, &aes_key) < 0)
    {
        //assert(false);
        return "";
    }
    std::string strRet;
    std::string data_bak = data;
    unsigned int data_length = data_bak.length();
    int padding = 0;
    if (data_bak.length() % AES_BLOCK_SIZE > 0)
    {
        padding =  AES_BLOCK_SIZE - data_bak.length() % AES_BLOCK_SIZE;
    }
    data_length += padding;
    while (padding > 0)
    {
        data_bak += '\0';
        padding--;
    }
    for(unsigned int i = 0; i < data_length/AES_BLOCK_SIZE; i++)
    {
        std::string str16 = data_bak.substr(i*AES_BLOCK_SIZE, AES_BLOCK_SIZE);
        unsigned char out[AES_BLOCK_SIZE];
        ::memset(out, 0, AES_BLOCK_SIZE);
        AES_encrypt((const unsigned char*)str16.c_str(), out, &aes_key);
        strRet += std::string((const char*)out, AES_BLOCK_SIZE);
    }
    return strRet;
}

std::string CDVEncrypt::replace(std::string str,std::string pattern,std::string dstPattern,int count=-1)
{
    std::string retStr="";
    string::size_type pos;
    
    int szStr=str.length();
    int szPattern=pattern.size();
    int i=0;
    int l_count=0;
    if(-1 == count) // replace all
    count = szStr;
    
    for(i=0; i<szStr; i++)
    {
        pos=str.find(pattern,i);
        
        if(std::string::npos == pos)
        break;
        if(pos < szStr)
        {
            std::string s=str.substr(i,pos-i);
            retStr += s;
            retStr += dstPattern;
            i=pos+pattern.length()-1;
            if(++l_count >= count)
            {
                i++;
                break;
            }
        }
    }
    retStr += str.substr(i);
    return retStr;
}

std::string CDVEncrypt::DecodeAES( const std::string& strPassword, const std::string& strData )
{
    AES_KEY aes_key;
    if(AES_set_decrypt_key((const unsigned char*)strPassword.c_str(), strPassword.length() * 8, &aes_key) < 0)
    {
        //assert(false);
        return "";
    }
    std::string strRet;
    for(unsigned int i = 0; i < strData.length()/AES_BLOCK_SIZE; i++)
    {
        std::string str16 = strData.substr(i*AES_BLOCK_SIZE, AES_BLOCK_SIZE);
        unsigned char out[AES_BLOCK_SIZE];
        ::memset(out, 0, AES_BLOCK_SIZE);
        AES_decrypt((const unsigned char*)str16.c_str(), out, &aes_key);
        strRet += std::string((const char*)out, AES_BLOCK_SIZE);
    }
    return strRet;
}

std::string CDVEncrypt::encrypt(const std::string cstr){

    int i;
    char destination[1024] = {0};
    
    string s = EncodeAES("uqi9mxr2pngdwuz2ww48a2jm",cstr);
    for (i = 0; i < strlen(s.c_str()); i++) {
        sprintf(destination, "%s%02x", destination, s[i]);
    }
    std::string res = replace(destination,"ffffff","");
    return res;
}
