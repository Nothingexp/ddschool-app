//
//  CDVWorkSo.m
//  UserClient
//
//  Created by woniu on 16/2/18.
//
//

#import "CDVBaseTools.h"
#import "CDVEncrypt.h"

@implementation CDVBaseTools

- (void)encrypt:(CDVInvokedUrlCommand*)command
{
    NSDictionary *params = [command.arguments objectAtIndex:0];
    if (!params)
    {
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"参数错误"] callbackId:command.callbackId];
    }
    
    NSString *ns_in_str = [params objectForKey:@"str"];
    std::string in_str =  [ns_in_str UTF8String];
    CDVEncrypt *cdvencrypt = new CDVEncrypt();
    std::string str = cdvencrypt->encrypt(in_str);
    
    NSString *ns_out_str= [NSString stringWithCString:str.c_str() encoding:[NSString defaultCStringEncoding]];

    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:ns_out_str];
    //CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"未安装微信"];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    
    
}

- (void)savePhotosAlbum:(CDVInvokedUrlCommand*)command
{
    NSDictionary *params = [command.arguments objectAtIndex:0];
    if (!params)
    {
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"参数错误"] callbackId:command.callbackId];
    }
    NSString *src = [params objectForKey:@"src"];
    
    NSURL *url=[NSURL URLWithString:src];
    UIImage *image =[[UIImage alloc]initWithData:[NSData dataWithContentsOfURL:url]];
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    
}

@end
