#import "HPKeyboardToolbar.h"
#import <QuartzCore/QuartzCore.h>

@implementation HPKeyboardToolbar

- (void) hide:(CDVInvokedUrlCommand*)command
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];

    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];

    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void) show:(CDVInvokedUrlCommand*)command
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];

    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];

}

// http://ios-blog.co.uk/tutorials/rich-text-editing-a-simple-start-part-1
- (void)keyboardWillShow:(NSNotification *)note {
    [self performSelector:@selector(removeBar) withObject:nil afterDelay:0];
}

- (void)removeBar {
    // Locate non-UIWindow.
    
    
    UIWindow* tempWindow;
    
    //Because we cant get access to the UIKeyboard throught the SDK we will just use UIView.
    //UIKeyboard is a subclass of UIView anyways
    UIView* keyboard;
    
    //NSLog(@"windows %d", [[[UIApplication sharedApplication]windows]count]);
    
//    //Check each window in our application
//    for(int c = 0; c < [[[UIApplication sharedApplication] windows] count]; c ++)
//    {
//        //Get a reference of the current window
//        tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:c];
//        
//        //Get a reference of the current view
//        for(int i = 0; i < [tempWindow.subviews count]; i++)
//        {
//            UIView* keyboardTmp = [tempWindow.subviews objectAtIndex:i];
//            //NSLog(@"view: %@, on index: %d, class: %@", [keyboard description], i, [[tempWindow.subviews objectAtIndex:i] class]);
//            if([[keyboardTmp description] hasPrefix:@"(lessThen)UIKeyboard"] == YES)
//            {
//                //If we get to this point, then our UIView "keyboard" is referencing our keyboard.
//                //return keyboard;
//                keyboard = tempWindow;
//                break;
//            }
//        }
//        
//        if(keyboard == nil){
//            for(UIView* potentialKeyboard in tempWindow.subviews)
//                // if the real keyboard-view is found, remember it.
//                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
//                    if([[potentialKeyboard description] hasPrefix:@"<UILayoutContainerView"] == YES)
//                        keyboard = potentialKeyboard;
//                }
//                else if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.2) {
//                    if([[potentialKeyboard description] hasPrefix:@"<UIPeripheralHost"] == YES)
//                        keyboard = potentialKeyboard;
//                }
//                else {
//                    if([[potentialKeyboard description] hasPrefix:@"<UIKeyboard"] == YES)
//                        keyboard = potentialKeyboard;
//                }
//        }
//        else{
//            break;
//        }
//    
//    }
//        
//    
//    if(keyboard != nil){
//        //keyboard.
//    }
    
    
    UIWindow *keyboardWindow = nil;
    for (UIWindow *testWindow in [[UIApplication sharedApplication] windows]) {
        if (![[testWindow class] isEqual:[UIWindow class]]) {
            keyboardWindow = testWindow;
            break;
        }
    }

    // Locate UIWebFormView
    for (UIView *possibleFormView in [keyboardWindow subviews]) {
        
        //UIInputSetContainerView
        //UIInputSetHostView
        if([[possibleFormView description] hasPrefix:@"<UIInputSetContainerView>"]){ //ios 8
            for (UIView* peripheralView in [possibleFormView subviews]) {

            if ([[peripheralView description] hasPrefix:@"<UIInputSetHostView"]) {
            for (UIView* peripheralView in [possibleFormView subviews]) {
                if ([peripheralView frame].origin.y == 0){
                    [[peripheralView layer] setOpacity:0.0];
                }
            }
            }
            }
        }
        
        else if ([[possibleFormView description] hasPrefix:@"<UIPeripheralHostView"]) {
            for (UIView* peripheralView in [possibleFormView subviews]) {
                
                // hides the backdrop (iOS 7)
                if ([[peripheralView description] hasPrefix:@"<UIKBInputBackdropView"]) {
                    //skip the keyboard background....hide only the toolbar background
                    if ([peripheralView frame].origin.y == 0){
                        [[peripheralView layer] setOpacity:0.0];
                    }
                }
                // hides the accessory bar
                if ([[peripheralView description] hasPrefix:@"<UIWebFormAccessory"]) {
                    // remove the extra scroll space for the form accessory bar
                    UIScrollView *webScroll;
                    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0) {
                        webScroll = [[self webView] scrollView];
                    } else {
                        webScroll = [[[self webView] subviews] lastObject];
                    }
                    CGRect newFrame = webScroll.frame;
                    newFrame.size.height += peripheralView.frame.size.height;
                    webScroll.frame = newFrame;
                    
                    // remove the form accessory bar
                    [peripheralView removeFromSuperview];
                }
                // hides the thin grey line used to adorn the bar (iOS 6)
                if ([[peripheralView description] hasPrefix:@"<UIImageView"]) {
                    [[peripheralView layer] setOpacity:0.0];
                }
            }
        }
    }
    
    

}


@end

