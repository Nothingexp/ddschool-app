//
//  CallPhone.m
//  Empresas PY
//
//  Created by Paul Von Schrottky on 1/16/14.
//
//

#import "HPCallPhone.h"

@implementation HPCallPhone

- (void)callNumber:(CDVInvokedUrlCommand *)command
{
    [self.commandDelegate runInBackground:^{
        self.command = command;
        NSString *phoneNumber = [command.arguments objectAtIndex:0];
        NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", phoneNumber]];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }];
}

@end