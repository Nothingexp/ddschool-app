触摸类事件：只会在移动设备中产生，对手的移动位置进行检测并做出响应

    onTouchCancel:
    onTouchEnd
    onTouchMove
    onTouchStart

2》键盘类事件：

    onKeyDown
    onKeyUp
    onKeyPress

3》剪切类事件

    onCopy
    onCut
    onPaste

4》 表单类事件

    onChange
    onInput
    onSubmit

5》 焦点类事件

    onFocus ： 获得焦点
    onBlur ： 失去焦点

6》UI元素： 元素或页面的滚动事件

    onScroll

7》滚动事件：监听滚动位置，方向

    onWheel

8》鼠标类事件：

    onClick
    onContextMenu ：右键，上下文菜单
    onDoubleClickc
    onMouseEnter
    onMouseDown
    onMouseLeave
    onMouseMove
    onMouseOut
    onMouseOver
    onMouseUp

9》鼠标拖拽事件： 上传内容

    onDrop
    onDrag
    onDragEnd
    onDragEnter
    onDragExit
    onDragLeave
    onDragOver
    onDragStart
