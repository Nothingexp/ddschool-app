
注意：接口中，已经赋值的参数不要改其值

首页：http://192.168.1.160/zy_server/index.php/api/user/article/index_list?data={"s":""}

家长获取孩子的班级：http://192.168.1.160/zy_server/index.php/api/user/school/parent_check_class?data={"g":"","s":""}

家长进入班级查看孩子：http://192.168.1.160/zy_server/index.php/api/user/school/parent_check_children?data={"g":"","s":""}

夏冬令营：
首页：http://192.168.1.160/zy_server/index.php/api/user/event/index?data={"g":"7","page":"1","s":""}
详细[同时调用]：
http://192.168.1.160/zy_server/index.php/api/user/event/show?data={"item_id":"","provide_id":"","s":""}
http://192.168.1.160/zy_server/index.php/api/user/event/order_role_index?data={"item_id":"","count":"","s":"","provide_id":""}
http://192.168.1.160/zy_server/index.php/api/user/event/comment_index?data={"g":"7","s":"","item_id":""}
我要报名：http://192.168.1.160/zy_server/index.php/api/user/order/store?data={"g":"7", "count":"1", "item_id":"", "token":"","s":""}

亲子游学：
首页：http://192.168.1.160/zy_server/index.php/api/user/event/index?data={"g":"8","page":"1","s":""}
详细[同时调用]：
http://192.168.1.160/zy_server/index.php/api/user/event/show?data={"item_id":"","provide_id":"","s":""}
http://192.168.1.160/zy_server/index.php/api/user/event/order_role_index?data={"item_id":"","count":"","s":"","provide_id":""}
http://192.168.1.160/zy_server/index.php/api/user/event/comment_index?data={"g":"8","s":"","item_id":""}
我要报名：http://192.168.1.160/zy_server/index.php/api/user/order/store?data={"g":"8", "count":"1", "item_id":"", "token":"","s":""}

学科资源库:
列表：http://192.168.1.160/zy_server/index.php/api/user/article/index?data={"c":"17","page":"1","s":"","token":""}
详细：http://192.168.1.160/zy_server/index.php/api/user/article/show?data={"id":"","s":"","token":""}

集团简介：http://192.168.1.160/zy_server/index.php/api/user/article/show?data={"id":"23","s":""}

学校架构：http://192.168.1.160/zy_server/index.php/api/user/article/show?data={"id":"18","s":""}

教师招聘：http://192.168.1.160/zy_server/index.php/api/user/article/show?data={"id":"20","s":""}

教师风采：
列表：http://192.168.1.160/zy_server/index.php/api/user/role/index?data={"g":"19","s":""}
http://192.168.1.160/zy_server/index.php/api/user/article/index?data={"c":"19","page":"1","s":""}
详细：http://192.168.1.160/zy_server/index.php/api/user/role/show?data={"id":"","s":""}
http://192.168.1.160/zy_server/index.php/api/user/article/show?data={"id":"","s":""}

招生专栏：http://192.168.1.160/zy_server/index.php/api/user/article/show?data={"id":"19","s":"","token":""}

学校视频：
列表：http://192.168.1.160/zy_server/index.php/api/user/article/index?data={"c":"2","page":"1","s":""}
详细：http://192.168.1.160/zy_server/index.php/api/user/article/show?data={"id":"","s":""}

校长信箱：
列表：
http://192.168.1.160/zy_server/index.php/api/user/note/index?data={"c":"1","page":"1","s":"","token":""}
保存：
http://192.168.1.160/zy_server/index.php/api/user/note/store?data={"c":"1","p":"0","s":"","token":"","content":""}

我要开课(增加、修改)：
页面：http://192.168.1.160/zy_server/index.php/api/user/role/extend_role_teacher?data={"token":"","s":""}
     L 根据班级获取班级下的科目：http://192.168.1.160/zy_server/index.php/api/user/event/assigned?data={"s":"","type":"0","data":"{\"c\":\"9999\",\"g\":\"9\"}"}
提交：http://192.168.1.160/zy_server/index.php/api/user/role/extend_role_teacher_register?data={"token":"","s":"","data":""}

课程中心：
首页：http://192.168.1.160/zy_server/index.php/api/user/event/index?data={"g":"9","s":"","type":"2"}
班级获取班级对应科目：http://192.168.1.160/zy_server/index.php/api/user/event/assigned?data={"s":"","type":"0","data":"{\"c\":\"9999\",\"g\":\"9\"}"}
筛选提交：http://192.168.1.160/zy_server/index.php/api/user/event/index?data={"g":"9","s":"1","type":"3","c":"{\"val_0\":\"年级id\",\"val_1\":\"科目标题\",\"val_2\":\"价格排序id\"}"}

热门课程排行榜：
列表：http://192.168.1.160/zy_server/index.php/api/user/event/index_popular?data={"g":"9","s":"","count":"10","type":"1","provide_id":""}
全部课程：http://192.168.1.160/zy_server/index.php/api/user/event/index?data={"g":"9","s":"","type":"1","c":""}

课程详情 + 我要报名：http://192.168.1.160/zy_server/index.php/api/user/event/show?data={"item_id":"","s":"","provide_id":""}
评论列表：http://192.168.1.160/zy_server/index.php/api/user/event/comment_index?data={"g":"9","s":"1","item_id":""}
热门教师排行榜：http://192.168.1.160/zy_server/index.php/api/user/event/index_popular?data={"g":"9","s":"","count":"10","type":"2","provide_id":""}
L XX老师课程列表：
              http://192.168.1.160/zy_server/index.php/api/user/event/index?data={"g":"9","s":"","rid":"","page":"","type":"0"}



我的[教师详情]：http://192.168.1.160/zy_server/index.php/api/user/role/show?data={"id":"","s":"","provide_id":""}
              L  我的授课：
              http://192.168.1.160/zy_server/index.php/api/user/event/index?data={"g":"9","s":"","token":"","page":"","type":"4"} 不用
              L  我的收入：
              http://192.168.1.160/zy_server/index.php/api/user/order/index?data={"token":"","g":"9","page":"","status":"7","s":"","","flag":"true"}


我的[家长详情]：http://192.168.1.160/zy_server/index.php/api/user/role/show?data={"id":"","s":"","provide_id":""}
              L  我的课程：
              http://192.168.1.160/zy_server/index.php/api/user/event/index?data={"g":"9","s":"","rid":"","type":"4"}
              L  我的订单：
              http://192.168.1.160/zy_server/index.php/api/user/order/index?data={"token":"","g":"9","page":"","status":"","s":"","flag":"false"}

创建评论：http://192.168.1.160/zy_server/index.php/api/user/event/comment_create?data={"g":"","s":"","item_id":"","content":""}


订单列表（查看自己下的订单）： http://192.168.1.160/zy_server/index.php/api/user/order/index?data={"token":"","g":"9","page":"","status":"7","s":""}
订单详细（查看自己下的订单）： http://192.168.1.160/zy_server/index.php/api/user/order/index?data={"token":"","page":"","s":""}
订单列表（查看自己被下的订单）： http://192.168.1.160/zy_server/index.php/api/user/order/index?data={"token":"","g":"9","page":"","status":"7","s":"","flag":"true"}
订单详细（查看自己被下的订单）： http://192.168.1.160/zy_server/index.php/api/user/order/index?data={"token":"","page":"","s":"","flag":"true"}












