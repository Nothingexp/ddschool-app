我的学校：
宗扬教育页面：school_index  //列表，轮播
学校架构页面：school_framework  //列表
教师风采页面：school_teachers //列表
教师风采详情页面：school_teachers_details
教师招聘页面：school_teachers_recruit //单页
学校视频页面：school_video  //列表
学校视频详情页面：school_video_detail //带视频播放
招生专栏页面：school_recruitment //单页
校长信箱页面：school_principal_mailbox //列表，评论框


集团简介：
集团简介页面：group_index //单页

我的班级：
我的班级页面：class_index //列表
我的班级详情页：class_detail //列表

课程中心：
课程中心页面： course_index//轮播，列表
认证教师平台页面：course_teacher_apply //表单，图片上传
全部课程页面：course_index_all //筛选，列表
热门课程页面：course_index_hot //列表
热门教师页面：course_teacher_hot //列表
教师课程列表页面：teacher_course_list //列表
课程详情页面：course_detail //单页
教师页面：course_teacher //单页
课程评论：course_comment //列表
教师个人资料页面：course_teacher_info //
教师个人收入页面：course_teacher_income //
学生家长页面：course_student_parents //

科学资源库：
科学资源库页面：science_resources //列表
科学资源库详情页面：science_resources_details //

亲子游学：
亲子游学页面：family_study //列表
亲子游学详情页面：family_study_detail //

夏冬令营：
夏冬令营页面：summer_cold_party//列表
夏冬令营详情页面：summer_cold_party_detail //

活动报名信息填写页面：confirm_sign_up


公共

订单报名页面：order_post //单页
订单支付页面：order_pay //

登录注册：
登录页面：sign_in
注册页面：sign_up

用户信息设置

系统设置

订单：
订单列表页：order_list //订单列表
订单详情页面：order_detail //订单详情


评论列表页面：comments //展示所有的评论，共用
28个页面





